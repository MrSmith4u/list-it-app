import { Component, OnInit } from '@angular/core';
import { ListingService } from './services/listing.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  title = 'list-it-app';
  blockNumber: number;

  constructor(private listingService: ListingService) {}

  ngOnInit(): void {
  }

  click() {
    this.listingService.connectAccount();
  }
}
