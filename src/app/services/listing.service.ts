import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

import Web3 from 'web3';
import Web3Modal from 'web3modal';

@Injectable({
	providedIn: 'root',
})
export class ListingService {

  private web3js: any;
  private provider: any;
  private accounts: any;
  private web3Modal: Web3Modal;

  private accountStatusSource = new Subject<any>();

	constructor() {

    const providerOptions = {
      /* See Provider Options Section */
    };
  
    this.web3Modal = new Web3Modal({
      network: 'http://127.0.0.1:7545', // optional
      cacheProvider: true, // optional
      providerOptions, // required
    });
  }

  async connectAccount() {
    this.web3Modal.clearCachedProvider();

    this.provider = await this.web3Modal.connect(); // set provider
    this.web3js = new Web3(this.provider); // create web3 instance
    this.accounts = await this.web3js.eth.getAccounts(); 
    this.accountStatusSource.next(this.accounts)
  }


}
