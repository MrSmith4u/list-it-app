// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;
pragma experimental ABIEncoderV2;

contract Listing {

  Item[] public itemsForSale;
  address public siteManager = msg.sender;
  uint private idCounter = 0;
  
  struct Item {
    uint id;
    string name;
    string description;
    uint price;
    string category;
    Status status;
    address payable owner;
    uint postedDate;
  }

  enum Status { 
    AVAILABLE, 
    SOLD 
  }

  event Posted(address indexed owner, uint id);
  event Purchased(address indexed buyer, address indexed owner, uint id);

  function postItem(string memory name, string memory description, uint price, string memory category, address payable owner) public returns (uint) {
    Item memory item = Item (idCounter, name, description, price, category, Status.AVAILABLE, owner, block.timestamp);
    itemsForSale.push(item);
    uint id = idCounter;
    idCounter = idCounter + 1;
    emit Posted(owner, id);
    return id;
  }

  function purchase(address buyer, uint id) payable external returns (bool) {
    Item memory item = getItem(id);
    item.owner.transfer(msg.value);
    item.status = Status.SOLD;
    itemsForSale[id] = item;
    emit Purchased(buyer, item.owner, id);
    return true;
  } 

  function getItems() external view returns (Item[] memory) {
    return itemsForSale;
  } 

  function getItem(uint id) public view returns (Item memory) {
    uint totalItems = itemsForSale.length;
    for(uint i = 0; i < totalItems; i++) {
      if(itemsForSale[i].id == id) {
        return itemsForSale[i];
      }
    }
  }
}
